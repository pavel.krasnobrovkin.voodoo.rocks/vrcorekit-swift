//
//  VRFlashView.swift
//  CoOpCubeAdmin
//
//  Created by Alice on 05/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRFlashView: UIView {
    
    @IBOutlet var messageLabel: UILabel?
    @IBOutlet var closeButton: UIButton?
    
    var fromNib = false
    
    var title: String = "" {
        didSet {
            setup()
        }
    }
    
    func setup() {
        
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        
        if !fromNib {
            backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        }
        
        if messageLabel == nil {
            messageLabel = UILabel()
            messageLabel?.frame = self.frame
            messageLabel?.textAlignment = .center
            messageLabel?.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
            messageLabel?.textColor = UIColor.white
            self.addSubview(messageLabel!)
        }
        messageLabel?.text = title
        
        if closeButton == nil {
            closeButton = UIButton()
            closeButton?.titleLabel?.textColor = UIColor.white
            closeButton?.setTitle("✕", for: .normal)
            closeButton?.sizeToFit()
            closeButton?.frame = CGRect(x: self.frame.size.width - closeButton!.frame.size.width,
                                        y: (self.frame.size.height - closeButton!.frame.size.height) / 2,
                                        width: closeButton!.frame.size.width,
                                        height: closeButton!.frame.size.height);
            
            self.addSubview(closeButton!)
        }
        closeButton?.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
    }
    
    func dismissWithDelay(delay: TimeInterval) {
        
        Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(dismiss), userInfo: nil, repeats: false)
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0
            }, completion: { result in
                self.removeFromSuperview()
        })
    }
}
