//
//  VRSharedObject.swift
//  CoOpCube
//
//  Created by Alice on 08/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRSharedObject: NSObject {
    
    fileprivate static var objects = [String: VRSharedObject]()
    
    required override public init() {
        super.init()
    }
    
    public static func shared() -> Self {
        return sharedObjectHelper()
    }
    
    fileprivate class func sharedObjectHelper<T: VRSharedObject>() -> T! {
        let className = NSStringFromClass(self as AnyClass)
        if let storedObject = VRSharedObject.objects[className] as? T {
            return storedObject
        }
        let newObject = T.init()
        VRSharedObject.objects[className] = newObject
        return newObject
    }
}
