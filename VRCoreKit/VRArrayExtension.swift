//
//  VRArrayExtension.swift
//  CoOpCube
//
//  Created by Egor Gaydamak on 07/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

extension Array where Element: Equatable {
    mutating func remove(_ object: Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
    
    mutating func removeObjectsIn(_ array: [Element]) {
        for object in array {
            self.remove(object)
        }
    }
}
