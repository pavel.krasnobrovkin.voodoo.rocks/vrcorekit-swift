Pod::Spec.new do |s|
  s.name             = "VRCoreKit"
  s.version          = "1.0"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-mobile@bitbucket.org/voodoo-mobile/vrcorekit-swift.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => "public@voodoo-mobile.com" }
  s.source           = { :git => s.homepage, branch:'master', :tag => s.version.to_s }

  s.platform     = :ios, '10.0'
  s.requires_arc = true

  s.source_files = 'VRCoreKit/*.swift'

    s.frameworks = 'Foundation', 'AVFoundation', 'UIKit'
    s.dependency 'RequestController', '~> 0.3.0'
    s.dependency 'SDWebImage', '~> 3.8.2'

end
