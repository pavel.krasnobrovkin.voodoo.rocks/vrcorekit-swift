//
//  VRStringExtension.swift
//  CoOpCube
//
//  Created by Alice on 06/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import Foundation

extension String
{
    public init(userDefaultsKey: String) {
        if UserDefaults.standard.value(forKey: userDefaultsKey) != nil {
            self.init(describing: UserDefaults.standard.value(forKey: userDefaultsKey)!)
            return
        }
        self.init()
    }
    
    public func serializeToUserDefaultsForKey(_ key: String) {
        UserDefaults.standard.set(self, forKey: key)
    }
    
    var underscoreToCamelCase: String {
        let items = self.components(separatedBy: "_")
        var camelCase = ""
        items.enumerated().forEach {
            camelCase += 0 == $0 ? $1 : $1.capitalized
        }
        return camelCase
    }
    
    var camelCaseToUnderscore: String {
        return replacingOccurrences(of: "([A-Z])", with: "_$1", options: String.CompareOptions.regularExpression, range: startIndex..<endIndex).lowercased()
    }
}
