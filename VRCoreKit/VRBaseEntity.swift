//
//  VREntity.swift
//  CoOpCube
//
//  Created by Alice on 09/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

open class VRBaseEntity: NSObject
{
    func getNestedPropertiesInfo() -> Dictionary<String, VRBaseEntity.Type>? {
        var result = Dictionary<String, VRBaseEntity.Type>()
        
        for (propertyName, value) in Mirror(reflecting: self).getChildrenRecursive()
        {
            let propertyType = getClassFromName(String(describing: type(of: value))) as? VRBaseEntity.Type
            if propertyType != nil {
                result[propertyName] = propertyType
            }
        }
        return result
    }
    
    func getAllPropertiesInfo() -> Dictionary<String, Any.Type>? {
        var result = Dictionary<String, Any.Type>()
        
        for (propertyName, value) in Mirror(reflecting: self).getChildrenRecursive()
        {
            let nonOptionalPropertyType = getClassFromName(String(describing: type(of: value))) as? Any.Type
            if nonOptionalPropertyType != nil {
                result[propertyName] = nonOptionalPropertyType
                continue
            }
            
            let propertyType = type(of: value)
            result[propertyName] = propertyType
        }
        return result
    }
}

func getClassFromName(_ className: String) -> Swift.AnyClass?
{
    // remove angle brackets if any
    let nameSubstrs = className.characters.split{$0 == "<" || $0 == ">"}.map(String.init)
    var propertyTypeName: String = ""
    for s in nameSubstrs {
        if s == "Optional" || s == "Array" { // array is stored like nested property - will be separated later
            continue
        }
        else if s == "Date" {
            return NSClassFromString("NSDate")
        }
        else {
            propertyTypeName = s
        }
    }
    
    // class is user created - add project name to class name
    let projectPath = Bundle.main.infoDictionary!["CFBundleExecutable"] as? String
    let finalPropertyTypeName = projectPath!.replacingOccurrences(of: " ", with: "_") + "." + propertyTypeName
    var propertyType: AnyClass? = NSClassFromString(finalPropertyTypeName)
    
    if propertyType == nil { // the class is in framework - add framework name to class name
        let frameworkProjectPath = Bundle(for: VRBaseEntity.self).infoDictionary!["CFBundleExecutable"] as? String
        let frameworkPropertyTypeName = frameworkProjectPath!.replacingOccurrences(of: " ", with: "_") + "." + propertyTypeName
        propertyType = NSClassFromString(frameworkPropertyTypeName)
    }
    
    return propertyType
}
