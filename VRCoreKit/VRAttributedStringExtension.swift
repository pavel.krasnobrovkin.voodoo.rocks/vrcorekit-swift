//
//  VRAttributedStringExtension.swift
//  Jobbio
//
//  Created by Olesya Kondrashkina on 28/11/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

extension NSAttributedString {
    
    public static func fromHtmlString(_ htmlString: String) -> Self {
        return htmlHelper(htmlString)
    }
    
    private class func htmlHelper<T: NSAttributedString>(_ htmlString: String) -> T {
        do {
            let str = try T(data: htmlString.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
            return str
        } catch {
            print(error)
        }
        return T.init(string: "")
    }
}
