//
//  VRValidatorSymbolCount.swift
//  Senate
//
//  Created by Alice on 14/10/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import Foundation

open class VRValidatorSymbolCount: NSObject, VRStringValidator {
    
    public var messageOnFail: String {
        get {
            return "Value should be at least " + String(symbolCount) + " symbols long"
        } set {
            self.messageOnFail = newValue
        }
    }
    var symbolCount = 0
    
    public func validate(_ value: String) -> Bool {
        let trimmedString = value.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedString.characters.count > symbolCount
    }
}
