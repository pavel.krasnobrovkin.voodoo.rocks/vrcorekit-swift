//
//  fef.swift
//  CoOpCube
//
//  Created by Alice on 15/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

//extension CGPath{
//    func forEach(callback: @escaping (CGPathElement)->() ){
//        typealias Callback = (CGPathElement) -> ()
//        
//        func apply(info: UnsafeMutablePointer<Void>, element:UnsafePointer<CGPathElement>){
//            let callback = UnsafeMutablePointer<Callback>(info)
//            callback.pointee(element.pointee)
//        }
//        
//        var calle = { callback($0) }
//        
//        withUnsafeMutablePointer(&calle) { pointer  in
//            CGPathApply(self,  pointer, apply)
//        }
//    }
//}
//
//extension UIBezierPath {
//    var length: CGFloat {
//        var pathLength:CGFloat = 0.0
//        var current = CGPointZero
//        var first   = CGPointZero
//        
//        self.cgPath.forEach { element in
//            pathLength += element.distance(to: current, startPoint: first)
//            
//            if element.type == .moveToPoint{
//                first = element.point
//            }
//            if element.type != .closeSubpath{
//                current = element.point
//            }
//        }
//        return pathLength
//    }
//}
//
//extension CGPathElement{
//    
//    var point: CGPoint{
//        switch type {
//        case .moveToPoint, .addLineToPoint:
//            return self.points[0]
//        case .addQuadCurveToPoint:
//            return self.points[1]
//        case .addCurveToPoint:
//            return self.points[2]
//        case .closeSubpath:
//            return CGRect.null.origin
//        }
//    }
//    
//    func distance(to point: CGPoint, startPoint: CGPoint ) -> CGFloat{
//        switch type {
//        case .moveToPoint:
//            return 0.0
//        case .closeSubpath:
//            return point.distance(to:startPoint)
//        case .addLineToPoint:
//            return point.distance(to:self.points[0])
//        case .addCurveToPoint:
//            return BezierCurveLength(point, c1: self.points[0], c2: self.points[1], p1: self.points[2])
//        case .addQuadCurveToPoint:
//            return BezierCurveLength(point, c1: self.points[0], p1: self.points[1])
//        }
//    }
//}
//
//extension CGPoint{
//    func distance(to:CGPoint) -> CGFloat{
//        let dx = pow(to.x - self.x,2)
//        let dy = pow(to.y - self.y,2)
//        return sqrt(dx+dy)
//    }
//}
//
//// Helper Functions
//
//func CubicBezierCurveFactors(t:CGFloat) -> (CGFloat,CGFloat,CGFloat,CGFloat){
//    let t1 = pow(1.0-t, 3.0)
//    let t2 = 3.0*pow(1.0-t,2.0)*t
//    let t3 = 3.0*(1.0-t)*pow(t,2.0)
//    let t4 = pow(t, 3.0)
//    
//    return  (t1,t2,t3,t4)
//}
//
//
//func QuadBezierCurveFactors(t:CGFloat) -> (CGFloat,CGFloat,CGFloat){
//    let t1 = pow(1.0-t,2.0)
//    let t2 = 2.0*(1-t)*t
//    let t3 = pow(t, 2.0)
//    
//    return (t1,t2,t3)
//}
//
//// Quadratic Bezier Curve
//func BezierCurve(t:CGFloat,p0:CGFloat,c1:CGFloat,p1:CGFloat) -> CGFloat{
//    let factors = QuadBezierCurveFactors(t)
//    return (factors.0*p0) + (factors.1*c1) + (factors.2*p1)
//}
//
//
//// Quadratic Bezier Curve
//func BezierCurve(t:CGFloat,p0:CGPoint,c1:CGPoint,p1:CGPoint) -> CGPoint{
//    let x = BezierCurve(t: t, p0: p0.x, c1: c1.x, p1: p1.x)
//    let y = BezierCurve(t: t, p0: p0.y, c1: c1.y, p1: p1.y)
//    return CGPoint(x: x, y: y)
//}
//
//
//
//// Cubic Bezier Curve
//func BezierCurve(t:CGFloat,p0:CGFloat, c1:CGFloat, c2:CGFloat, p1:CGFloat) -> CGFloat{
//    let factors = CubicBezierCurveFactors(t)
//    return (factors.0*p0) + (factors.1*c1) + (factors.2*c2) + (factors.3*p1)
//}
//
//
//// Cubic Bezier Curve
//func BezierCurve(t: CGFloat, p0:CGPoint, c1:CGPoint, c2: CGPoint, p1: CGPoint) -> CGPoint{
//    let x = BezierCurve(t: t, p0: p0.x, c1: c1.x, c2: c2.x, p1: p1.x)
//    let y = BezierCurve(t: t, p0: p0.y, c1: c1.y, c2: c2.y, p1: p1.y)
//    return CGPoint(x: x, y: y)
//}
//
//
//// Cubic Bezier Curve Length
//func BezierCurveLength(p0:CGPoint,c1:CGPoint, c2:CGPoint, p1:CGPoint) -> CGFloat{
//    let steps = 12 // on greater samples, more presicion
//    
//    var current  = p0
//    var previous = p0
//    var length:CGFloat = 0.0
//    
//    for i in 1...steps{
//        let t = CGFloat(i) / CGFloat(steps)
//        current = BezierCurve(t: t, p0: p0, c1: c1, c2: c2, p1: p1)
//        length += previous.distance(to: current)
//        previous = current
//    }
//    
//    return length
//}
//
//
//// Quadratic Bezier Curve Length
//func BezierCurveLength(p0:CGPoint,c1:CGPoint, p1:CGPoint) -> CGFloat{
//    let steps = 12 // on greater samples, more presicion
//    
//    var current  = p0
//    var previous = p0
//    var length:CGFloat = 0.0
//    
//    for i in 1...steps{
//        let t = CGFloat(i) / CGFloat(steps)
//        current = BezierCurve(t: t, p0: p0, c1: c1, p1: p1)
//        length += previous.distance(to: current)
//        previous = current
//    }
//    return length
//}
