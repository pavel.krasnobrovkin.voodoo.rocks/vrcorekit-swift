//
//  VRRequestManager.swift
//
//  Created by Alice on 25/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD

open class VRRequestManager: VRSharedObject {
    
    public var exceptionHandler: VRNetworkExceptionHandlerProtocol?
    
    public func execute(_ request: VRBaseRequest, showProgressHUD: Bool = true, showProgressResultHUD: Bool = true, callback: (([String: Any]?, Error?) -> Void)?) {
        
        if showProgressHUD {
            HUD.show(.progress)
        }
        
        let payload = request.payload
        print("\n\n<<=== Request ===>>\n" + request.method() + "\n" + payload.toFormattedString())
        
        let escapedAddress = request.method().addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let alamofireRequest = Alamofire.request(escapedAddress!, method: .post, parameters: payload, encoding: JSONEncoding.default)
        alamofireRequest.validate()
        alamofireRequest.response { dataResponse in
            
            if let data = dataResponse.data, let utf8Text = String(data: data, encoding: .utf8), let resultDictionary = VRRequestManager.shared().convertJSONStringToDictionary(jsonString: utf8Text) {
                
                print("\n\n<<=== Response ===>>\n" + request.method() + resultDictionary.toFormattedString())
                
                let baseResponse = VRBaseResponse(dictionary: resultDictionary)
                if !baseResponse.success && baseResponse.exception != nil {
                    DispatchQueue.main.async(execute: {
                        self.exceptionHandler?.handleException(baseResponse.exception!)
                    })
                    
                    if showProgressResultHUD {
                        HUD.flash(.error, delay: 0.1)
                    } else {
                        HUD.hide()
                    }
                    return
                }
                
                if showProgressResultHUD {
                    HUD.flash(.success, delay: 0.1)
                } else {
                    HUD.hide()
                }
                
                if callback != nil {
                    DispatchQueue.main.async(execute: {
                        callback!(resultDictionary, dataResponse.error)
                    })
                }
            }
            else {
                
                if showProgressResultHUD {
                    HUD.flash(.error, delay: 0.1)
                } else {
                    HUD.hide()
                }
                
                print("\n\n<<=== Empty Response ===>>\n" + request.method() + (dataResponse.error != nil ? (dataResponse.error!).localizedDescription : ""))
                
                if (dataResponse.error!).localizedDescription == "The network connection was lost." {
                    DispatchQueue.main.async(execute: {
                        self.execute(request, callback: callback)
                    })
                    return
                }
                
                if (dataResponse.error!).localizedDescription == "The Internet connection appears to be offline." {
                    VRFlashManager.shared().showFlash((dataResponse.error!).localizedDescription)
                    return
                }
                
                if callback != nil {
                    DispatchQueue.main.async(execute: {
                        callback!(nil, dataResponse.error)
                    })
                }
            }
        }
    }
    
    func convertJSONStringToDictionary(jsonString: String) -> [String:AnyObject]? {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
