//
//  NetworkExceptionHandlerProtocol.swift
//  CoOpCube
//
//  Created by Olesya Kondrashkina on 23/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import Foundation

public protocol VRNetworkExceptionHandlerProtocol {
    func handleException(_ exception: VRException)
}
